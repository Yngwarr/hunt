﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class Maze : MonoBehaviour
{
	public Grid grid;

	public GameObject wallPrefab;
	public GameObject floorPrefab;
	public GameObject playerPrefab;

	static readonly Vector2Int[] Dirs = {
		Vector2Int.left,
		Vector2Int.right,
		Vector2Int.up,
		Vector2Int.down
	};
	MazeGrid mazeGrid;
	
	void Generate(int width, int height) {
		var w = width*2 + 1;
		var h = height*2 + 1;
		mazeGrid = new MazeGrid(w, h);
		
		Vector2Int curr = new Vector2Int(Random.Range(0, width)*2 + 1, Random.Range(0, height)*2 + 1);

        var availableDirs = new List<Vector2Int>();
        while (true) {
			availableDirs.Clear();
			mazeGrid.SetCell(curr, MazeGrid.Cell.EMPTY);
			foreach (var dir in Dirs) {
				var next = curr + (dir * 2);
				if (mazeGrid.OutOfBounds(next)) continue;
				if (mazeGrid.IsEmpty(next)) continue;
				availableDirs.Add(dir);
			}
			
			if (availableDirs.Count == 0) {
				var found = false;
				for (int y = 1; !found && y < mazeGrid.height; y += 2) {
					for (int x = 1; !found && x < mazeGrid.width; x += 2) {
						if (mazeGrid.IsEmpty(x, y)) continue;
						foreach (var d in Dirs) {
							curr = new Vector2Int(x, y);
							if (!mazeGrid.IsEmpty(curr + d*2)) continue;
							mazeGrid.SetCell(curr + d, MazeGrid.Cell.EMPTY);
							found = true;
							break;
						}
					}
				}
				if (!found) break;
			} else {
				var chosenDir = availableDirs[Random.Range(0, availableDirs.Count)];
				mazeGrid.SetCell(curr + chosenDir, MazeGrid.Cell.EMPTY);
				curr += chosenDir * 2;
				mazeGrid.SetCell(curr, MazeGrid.Cell.EMPTY);
			}
		}
	}

	GameObject InstantiateAt(GameObject original, Vector3Int position) {
		return Instantiate(original, grid.GetCellCenterWorld(position), Quaternion.identity);
	}

	void Draw() {
		for (int y = 0; y < mazeGrid.height; ++y) {
			for (int x = 0; x < mazeGrid.width; ++x) {
				var cell = mazeGrid.GetCell(x, y);
				var pos = new Vector3Int(x, y, 0);

				switch (cell) {
					case MazeGrid.Cell.WALL:
						var tile = InstantiateAt(wallPrefab, pos);
						if (mazeGrid.IsBoundary(x, y)) {
                            tile.GetComponent<Wall>().destructible = false;
                        } else {
							InstantiateAt(floorPrefab, pos);
						}
						break;
					case MazeGrid.Cell.EMPTY:
						InstantiateAt(floorPrefab, pos);
						break;
					default:
						Debug.LogWarning($"Unknown cell type '{cell}'.");
						continue;
				}
			}
		}
	}

    // Start is called before the first frame update
    void Start() {
        Generate(25, 11);
		Draw();
		InstantiateAt(playerPrefab, (Vector3Int) mazeGrid.RandomEmpty());
    }
}
