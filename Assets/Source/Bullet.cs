﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    ParticleSystem particles;
    // Component.collider is deprecated but still here -_-
    Collider2D _collider;

    void Start() {
        particles = GetComponent<ParticleSystem>();
        _collider = GetComponent<Collider2D>();
    }

    void OnCollisionEnter2D(Collision2D col) {
        _collider.enabled = false;
        particles.Stop();
    }
}
