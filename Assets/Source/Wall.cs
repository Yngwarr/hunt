﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {
    public bool destructible = true;

    void OnCollisionEnter2D(Collision2D col) {
        if (!destructible) return;
        if (!col.gameObject.GetComponent<Bullet>()) return;
        Destroy(gameObject);
    }
}
