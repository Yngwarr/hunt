﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AmmoLabel : MonoBehaviour
{
    TMP_Text text;

    public void UpdateText(int ammo_left) {
        text.text = $"Ammo: {ammo_left}";
    }

    // Start is called before the first frame update
    void Start() {
        text = GetComponent<TMP_Text>();
        EventManager.Get().playerShot.AddListener(new UnityAction<int>(UpdateText));
    }
}
