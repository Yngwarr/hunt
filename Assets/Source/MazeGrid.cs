﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class MazeGrid
{
	public enum Cell { WALL, EMPTY, NULL };
	public readonly int width;
	public readonly int height;
	Cell[][] grid;
	
	public MazeGrid(int w, int h) {
		width = w;
		height = h;
		// walls by default
		grid = new Cell[w][];
		for (int i = 0; i < w; ++i) {
			grid[i] = new Cell[h];
		}
	}
	
	public bool OutOfBounds(Vector2Int point) {
		return OutOfBounds(point.x, point.y);
	}
	
	public bool OutOfBounds(int x, int y) {
		return x < 0 || x >= width
			|| y < 0 || y >= height;
	}
	
	public bool IsEmpty(Vector2Int point) {
		return IsEmpty(point.x, point.y);
	}
	
	public bool IsEmpty(int x, int y) {
		return !OutOfBounds(x, y) && GetCell(x, y) == Cell.EMPTY;
	}

	public bool IsBoundary(int x, int y) {
		return x == 0 || y == 0 || x == width - 1 || y == height - 1;
	}
	
	public void SetCell(Vector2Int point, Cell value) {
		SetCell(point.x, point.y, value);
	}
	
	public void SetCell(int x, int y, Cell value) {
		if (OutOfBounds(x, y)) return;
		grid[x][y] = value;
	}
	
	public Cell GetCell(Vector2Int point) {
		return GetCell(point.x, point.y);
	}
	
	public Cell GetCell(int x, int y) {
		return OutOfBounds(x, y) ? Cell.NULL : grid[x][y];
	}

	public Vector2Int RandomEmpty() {
		// odd cells are guaranteed to be empty by Maze.Generate
		return new Vector2Int(Random.Range(0, (width - 1)/2)*2 + 1, Random.Range(0, (height - 1)/2)*2 + 1);
	}
	
	public void Print() {
		var sb = new StringBuilder();
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				sb.Append(IsEmpty(x, y) ? '.' : '#');
			}
			sb.AppendLine();
		}
		Debug.Log(sb.ToString());
	}
}
