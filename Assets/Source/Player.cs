﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GameObject aim;

    Rigidbody2D rigid;
    Animator anim;

    const float EPS = .2f;
    readonly int paramRight;

    Vector2 input;
    Vector2 look;
    float lookAngle;

    int ammo = 15;

    public Player() : base() {
        paramRight = Animator.StringToHash("Right");
    }

    void Shoot() {
        if (ammo == 0) return;
        
        var bullet = Instantiate(bulletPrefab);
        var bulletRigid = bullet.GetComponent<Rigidbody2D>();
        bulletRigid.position = rigid.position;
        bulletRigid.rotation = lookAngle;
        bulletRigid.AddForce(look.normalized * 10, ForceMode2D.Impulse);

        ammo--;
        EventManager.Get().playerShot.Invoke(ammo);
    }

    // Start is called before the first frame update
    void Start() {
        rigid = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        look = aim.transform.localPosition.normalized;
    }

    void Update() {
        input.x = Input.GetAxisRaw("Horizontal");
        input.y = Input.GetAxisRaw("Vertical");

        Vector2 lookInput;
        lookInput.x = Input.GetAxisRaw("HorizontalTurn");
        lookInput.y = Input.GetAxisRaw("VerticalTurn");

        if (!Mathf.Approximately(lookInput.magnitude, 0)) {
            look = lookInput;
            lookAngle = Vector2.SignedAngle(Vector2.right, look);
            aim.transform.localPosition = look.normalized;
            aim.transform.rotation = Quaternion.Euler(0, 0, lookAngle);
        }

        if (Input.GetButtonDown("Fire")) {
            Shoot();
        }

        if (input.x < 0) {
            anim.SetBool(paramRight, false);
        } else if (input.x > 0) {
            anim.SetBool(paramRight, true);
        }

        anim.SetBool("Walk", rigid.velocity.magnitude > EPS);
    }

    void FixedUpdate() {
        rigid.AddForce(new Vector2(input.x, input.y).normalized, ForceMode2D.Impulse);
    }
}
