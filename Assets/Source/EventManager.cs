﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class EventManager
{
    private static EventManager eventManager;

    public static EventManager Get() {
        if (eventManager == null) {
            eventManager = new EventManager();
        }
        return eventManager;
    }

    // TODO make a generic version with dynamically added events
    public UnityEvent<int> playerShot = new UnityEvent<int>();
}
