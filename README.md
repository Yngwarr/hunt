# Hunt: Unity edition

This is a slightly modernized version of a good-old BSD game called [hunt](https://man.openbsd.org/OpenBSD-current/man6/hunt.6).

## Features

* random maze generation with [hunt-and-kill algorithm](http://weblog.jamisbuck.org/2011/1/24/maze-generation-hunt-and-kill-algorithm);
* a walking and shooting player-controlled character;
* destructible walls.

## Controls

### Keyboard

* WASD or arrow keys to move; player aims in a direction of movement;
* space to shoot.

### Xbox-compatible gamepad

* Left stick to move;
* Right stick to aim;
* Right shoulder to shoot.

## Dependencies

The game can be built with Unity 2020.1.

## Credits

Most of the sprites are taken from a watabou's open-source game [Pixel Dungeon](https://github.com/watabou/pixel-dungeon/).

All the coding and Unity-ing is done by me, [Yngwarr](https://hardweird.ru).

***

## TODO list

* limited ~~ammo supply and~~ health;
* randomly placed pickups for extra ammo and health;
* 4-side aim lock (just like in the original game);
* strafing with a keyboard;
* AI-controled bots;
* fog of war;
* multiplayer mode;
* different weapons;
* animated explosions.
